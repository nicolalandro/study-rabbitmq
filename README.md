# Rabbit MQ

```
python3.10 -m venv venv
source venv/bin/activate
# or for fish shell source venv/bin/activate.fish
pip install -r requirements

docker-compose up

python src/publisher.py
>  [x] Sent 'Hello World!'

python src/subscriber.py
>  [*] Waiting for messages. To exit press CTRL+C
>  [x] Received b'Hello World!'
# press CTRL+C
```

# References
* [RabbitMQ](https://www.rabbitmq.com/): message queue 
* [RabbitMQ with python](https://www.rabbitmq.com/tutorials/tutorial-one-python.html): tutorial to use rabbit mq with python
